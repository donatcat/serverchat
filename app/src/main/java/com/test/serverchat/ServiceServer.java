package com.test.serverchat;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import static com.test.serverchat.ApplicationClass.CHANNEL_DESC;
import static com.test.serverchat.ApplicationClass.CHANNEL_ID;
import static com.test.serverchat.ApplicationClass.CHANNEL_NAME;

public class ServiceServer extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        Notification mnotification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(CHANNEL_NAME)
                .setContentText(CHANNEL_DESC)
                .setSmallIcon(
                        R.drawable.ic_launcher_foreground
                )
                // .setContentIntent(pendingIntent)
                // .setContent(view)
                .build();

        startForeground(1, mnotification);

        int port = 10000;
        Handler handler = new Handler();
        // Runnable r = new Runnable() {
        //     public void run() {
        //ChatServer server = new ChatServer(port);
        //server.execute();
        ChatServerThread server = new ChatServerThread(port);
        server.start();
        //      }
        //  };
        //  handler.postDelayed(r, 500);
    }
}
